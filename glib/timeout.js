#!/usr/bin/env seed

GLib = imports.gi.GLib;

count = 0;

function test(){
	count++;
	print("Hello from timeout number " + count);
	if (count == 5)
		Seed.quit();
	return true;
}

GLib.timeout_add(0, 500, test);

mainloop = new GLib.MainLoop.c_new(null, false);
mainloop.run();
